astrometry.net (0.97+dfsg-1) unstable; urgency=medium

  * New upstream version 0.97+dfsg
  * Rediff patches

 -- Ole Streicher <olebole@debian.org>  Tue, 03 Dec 2024 13:34:23 +0100

astrometry.net (0.96+dfsg-2) unstable; urgency=medium

  * Remove horizons.py from Python package (Closes: #1084555)

 -- Ole Streicher <olebole@debian.org>  Tue, 19 Nov 2024 09:08:38 +0100

astrometry.net (0.96+dfsg-1) unstable; urgency=medium

  * New upstream version 0.96+dfsg
  * Rediff patches

 -- Ole Streicher <olebole@debian.org>  Sat, 14 Sep 2024 21:07:13 +0200

astrometry.net (0.95+dfsg-1) unstable; urgency=medium

  * New upstream version 0.95+dfsg
  * Rediff patches
  * Push Standards-Version to 4.7.0. No changes needed

 -- Ole Streicher <olebole@debian.org>  Mon, 27 May 2024 08:17:36 +0200

astrometry.net (0.93+dfsg-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.  Closes: #1061922

 -- Steve Langasek <vorlon@debian.org>  Wed, 28 Feb 2024 01:52:20 +0000

astrometry.net (0.93+dfsg-1) unstable; urgency=medium

  * New upstream version 0.93+dfsg
  * Rediff patches
  * Push Standards-Version to 4.6.2. No changes

 -- Ole Streicher <olebole@debian.org>  Fri, 23 Dec 2022 21:06:04 +0100

astrometry.net (0.92+dfsg-1) unstable; urgency=medium

  * New upstream version 0.92+dfsg
  * Rediff patches

 -- Ole Streicher <olebole@debian.org>  Fri, 02 Dec 2022 20:47:35 +0100

astrometry.net (0.91+dfsg-1) unstable; urgency=medium

  * New upstream version 0.91+dfsg
  * Rediff patches
  * Include netpbm files from netpbm subdir

 -- Ole Streicher <olebole@debian.org>  Fri, 05 Aug 2022 14:10:45 +0200

astrometry.net (0.89+dfsg-2) unstable; urgency=medium

  [ Jenkins ]
  * Set upstream metadata fields: Bug-Submit, Repository, Repository-Browse.
  * Remove obsolete field Name from debian/upstream/metadata

  [ Ole Streicher ]
  * Switch build depends on libnetpbm10-dev to libnetpbm-dev (Closes: #1003165)
  * Push Standards-Version to 4.6.1. No changes needed
  * Fix copyright for kdtree files (Closes: #983306)

 -- Ole Streicher <olebole@debian.org>  Sun, 31 Jul 2022 16:12:38 +0200

astrometry.net (0.89+dfsg-1) unstable; urgency=medium

  * New upstream version 0.89+dfsg
  * Drop don-t-try-to-use-128-bit-types-on-32-bit-platforms.patch

 -- Ole Streicher <olebole@debian.org>  Sat, 29 Jan 2022 09:53:19 +0100

astrometry.net (0.88+dfsg-1) unstable; urgency=medium

  * New upstream version 0.88+dfsg
  * Rediff patches
  * Push Standards-Version to 4.6.0. No changes required
  * Update names of sample stars to be found on CI test
  * Don't try to use 128-bit types on 32-bit platforms

 -- Ole Streicher <olebole@debian.org>  Mon, 17 Jan 2022 14:08:13 +0100

astrometry.net (0.85+dfsg-1) unstable; urgency=medium

  * New upstream version 0.85+dfsg
  * Rediff patches

 -- Ole Streicher <olebole@debian.org>  Mon, 16 Aug 2021 20:10:11 +0200

astrometry.net (0.82+dfsg-1) unstable; urgency=medium

  * New upstream version 0.82+dfsg
  * Rediff patches
  * Push Standards-Version to 4.5.1. No changes needed.
  * Push dh-compat to 13. Ignore missing files in dh_missing.
  * Add "Rules-Requires-Root: no" to d/control

 -- Ole Streicher <olebole@debian.org>  Tue, 19 Jan 2021 11:28:47 +0100

astrometry.net (0.80+dfsg-1) unstable; urgency=medium

  * New upstream version 0.80+dfsg. Rediff patches
  * Remove removal of ngc2000 from d/copyright: replaced by openngc
  * Push Standards-Version to 4.5.0. No changes needed.

 -- Ole Streicher <olebole@debian.org>  Tue, 28 Apr 2020 09:13:39 +0200

astrometry.net (0.78+dfsg-3) unstable; urgency=low

  * Adjust new source-extractor name (Closes: #944802)
  * Adjust CI test tmpdir name

 -- Ole Streicher <olebole@debian.org>  Fri, 15 Nov 2019 17:39:42 +0100

astrometry.net (0.78+dfsg-2) unstable; urgency=low

  * Build only for default Python 3 version (Closes: #943632)
  * Push Standards-Version to 4.4.1. No changes needed
  * Remove d/compat

 -- Ole Streicher <olebole@debian.org>  Tue, 29 Oct 2019 16:25:03 +0100

astrometry.net (0.78+dfsg-1) unstable; urgency=low

  * Add tests on salsa
  * New upstream version 0.78+dfsg. Rediff patches
  * Push Standards-Version to 4.4.0. No changes required.
  * Push compat to 12

 -- Ole Streicher <olebole@debian.org>  Tue, 23 Jul 2019 17:35:20 +0200

astrometry.net (0.76+dfsg-3) unstable; urgency=low

  * Replace EXIF.py by python3-exif package

 -- Ole Streicher <olebole@debian.org>  Tue, 04 Dec 2018 08:38:12 +0100

astrometry.net (0.76+dfsg-2) unstable; urgency=low

  * Switch python package to Python 3

 -- Ole Streicher <olebole@debian.org>  Wed, 28 Nov 2018 09:19:46 +0100

astrometry.net (0.76+dfsg-1) unstable; urgency=low

  * New upstream version 0.76+dfsg. Rediff patches
  * Push Standards-Version to 4.2.1. Change some URLs to https

 -- Ole Streicher <olebole@debian.org>  Tue, 04 Sep 2018 17:34:26 +0200

astrometry.net (0.75+dfsg-1) unstable; urgency=low

  * New upstream version 0.75+dfsg. Rediff patches
  * Push Standards-Version to 4.1.5. No changes needed.

 -- Ole Streicher <olebole@debian.org>  Mon, 16 Jul 2018 10:40:07 +0200

astrometry.net (0.74+dfsg-1) unstable; urgency=low

  * Update VCS fields to use salsa.d.o
  * New upstream version 0.74+dfsg. Rediff patches
  * Push Standards-Version top 4.1.4. Change d/copyright format URL to https
  * Push Compat to 11

 -- Ole Streicher <olebole@debian.org>  Fri, 13 Apr 2018 09:45:22 +0200

astrometry.net (0.73+dfsg-1) unstable; urgency=low

  * New upstream version 0.73+dfsg. Rediff patches
  * Push Standards-Version to 4.1.1. No changes required
  * Drop libcfitsio3-dev alternative build dependency

 -- Ole Streicher <olebole@debian.org>  Tue, 21 Nov 2017 11:14:12 +0100

astrometry.net (0.72+dfsg-1) unstable; urgency=low

  * New upstream version 0.72+dfsg
  * Dropped patch integrated in upstream

 -- Ole Streicher <olebole@debian.org>  Fri, 21 Jul 2017 13:46:54 +0200

astrometry.net (0.71+dfsg-2) unstable; urgency=low

  * Make the solver run on astropy 2.0 and pyfits 3.4

 -- Ole Streicher <olebole@debian.org>  Mon, 10 Jul 2017 14:20:17 +0200

astrometry.net (0.71+dfsg-1) unstable; urgency=low

  * New upstream version 0.71+dfsg. Rediff patches
  * Push Standards-Version to 4.0.0. No changes needed.

 -- Ole Streicher <olebole@debian.org>  Wed, 05 Jul 2017 09:27:00 +0200

astrometry.net (0.70+dfsg-1) unstable; urgency=medium

  * New upstream version 0.70+dfsg. Closes: #848505
  * Rediff patches

 -- Ole Streicher <olebole@debian.org>  Thu, 22 Dec 2016 13:34:59 +0100

astrometry.net (0.69+dfsg-1) unstable; urgency=medium

  * New upstream version 0.69+dfsg. Rediff patches

 -- Ole Streicher <olebole@debian.org>  Fri, 02 Dec 2016 14:06:06 +0100

astrometry.net (0.68+dfsg-1) unstable; urgency=medium

  * Update standards-version (no changes) and VCS fields
  * Add Bug-Database
  * New upstream version 0.68+dfsg. Rediff patches

 -- Ole Streicher <olebole@debian.org>  Thu, 01 Dec 2016 21:37:27 +0100

astrometry.net (0.67+dfsg-1) unstable; urgency=low

  * New upstream version

 -- Ole Streicher <olebole@debian.org>  Mon, 25 Jan 2016 21:17:52 +0100

astrometry.net (0.66+dfsg-1) unstable; urgency=low

  * New upstream version

 -- Ole Streicher <olebole@debian.org>  Mon, 18 Jan 2016 09:45:01 +0100

astrometry.net (0.65+dfsg-2) unstable; urgency=low

  * Update build dependencies for libpng, change libpng1.2-dev to libpng-dev.
    Closes: #810180

 -- Ole Streicher <olebole@debian.org>  Thu, 07 Jan 2016 13:56:13 +0100

astrometry.net (0.65+dfsg-1) unstable; urgency=low

  * New upstream version

 -- Ole Streicher <olebole@debian.org>  Tue, 15 Dec 2015 09:32:22 +0100

astrometry.net (0.64+dfsg-2) unstable; urgency=low

  [ Bas Couwenberg ]
  * Update build dependencies for GSL 2, change libgsl0-dev to libgsl-dev.
    Closes: #807182

 -- Ole Streicher <olebole@debian.org>  Mon, 07 Dec 2015 09:25:41 +0100

astrometry.net (0.64+dfsg-1) unstable; urgency=low

  * New upstream version
  * Fix processing of FITS files

 -- Ole Streicher <olebole@debian.org>  Mon, 26 Oct 2015 23:10:43 +0100

astrometry.net (0.63+dfsg-4) unstable; urgency=low

  * Recommend sextractor instead of depending on it.
  * Include CI test with a public domain image

 -- Ole Streicher <olebole@debian.org>  Fri, 23 Oct 2015 11:02:10 +0200

astrometry.net (0.63+dfsg-3) unstable; urgency=low

  * Don't use -march=native (correct previous d/changelog entry)

 -- Ole Streicher <olebole@debian.org>  Wed, 07 Oct 2015 10:39:09 +0200

astrometry.net (0.63+dfsg-2) unstable; urgency=low

  * Fix FTBS on s390x

 -- Ole Streicher <olebole@debian.org>  Sun, 04 Oct 2015 13:35:33 +0200

astrometry.net (0.63+dfsg-1) unstable; urgency=low

  * Initial release. Closes: #793461

 -- Ole Streicher <olebole@debian.org>  Sun, 13 Sep 2015 15:23:09 +0200
